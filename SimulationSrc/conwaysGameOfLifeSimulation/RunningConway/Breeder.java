package conwaysGameOfLifeSimulation.RunningConway;

import java.util.Arrays;

import conwaysGameOfLifeSimulation.Breeding.Gamete;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import main.ConwayMain;

public class Breeder {
	private DNAData specimens;
	private int boardSize;
	private int chunkSize;
	private int [] fitnessScores;
	private int [] fitnessScoresSorted;
	private int populationSize;
	private DNAData newSpecimens;
	private double mutationRate;

	/*
	 * Breeder constructor
	 * @param: specimens is of the type DNAData, a holder for our individual in the population.
	 * @param: maxBreeds is the maximum number of breeds for the best performing specimen.
	 * @param: chunkSize is the square root of the size of a gene. Genes are defined as square portions of a specimens board.
	 * @param: boardSize is the X and Y length of our board and its 2D array representation.
	 * @param: fitnessScores is an array with the fitness scores of the specimens.
	 * @param: fitnessScoresSorted is a sorted version of the fitness scores array.
	 * @param: mutationRate is the rate of mutation in the gamete
	 */
	public Breeder(DNAData specimens, int chunkSize, int boardSize, int [] fitnessScores, int [] fitnessScoresSorted, double mutationRate){
		this.specimens = specimens;
		this.chunkSize = chunkSize;
		this.boardSize = boardSize;
		this.fitnessScores = fitnessScores;
		this.fitnessScoresSorted = fitnessScoresSorted;
		this.populationSize = fitnessScores.length;
		this.newSpecimens = specimens;
		this.mutationRate = mutationRate;
	}

	/*
	 * Method to create and get gamete A.
	 * @param: a is the DNA of the first specimen.
	 * @return: a gamete of the first specimen. This will become a chromosome of an offspring.
	 */
	private Gamete getGameteA(DNA a) {
		return new Gamete(a, a.getA().length, boardSize, chunkSize, mutationRate);
	}

	/*
	 * Method to create and get gamete A.
	 * @param: b is the DNA of the second specimen.
	 * @return: a gamete of the second specimen. This will become a chromosome of an offspring.
	 */
	private Gamete getGameteB(DNA b) {
		return new Gamete(b, b.getA().length, boardSize, chunkSize, mutationRate);
	}

	/*
	 * Method to order the specimens based on their previously calculated and sorted viabilities.
	 */
	private void orderDNA() {
		DNA [] data = specimens.getDnaData();
		DNA [] newData = new DNA [populationSize];
		for(int i = 0; i < populationSize; ++i) {
			for(int j = 0; j < populationSize; ++j) {
				if(fitnessScores[j] == fitnessScoresSorted[i]) {
					newData[i] = data[j];
					break;
				}
			}
		}
		this.specimens = new DNAData (newData);
	}

	/*
	 * Method to get the newly created specimens from the Breed method.
	 * @return: newSpecimens is of the type DNADat and holds the specimens for the next
	 * generation.
	 */
	public DNAData getNewSpecimens() {
		return newSpecimens;
	}

	/*
	 * Method to bread the current generation to create the next.
	 */
	public void Breed(int survivors, int breedsPerTopGun) {
		orderDNA();
		int counter = 0;
		int index = populationSize - 1;
		DNA [] newData = new DNA[populationSize];
		for(int i = 0; i < survivors; ++i) {
			DNA nextUp = specimens.getDnaData()[index-i];
			boolean [] A = Arrays.copyOf(nextUp.getA(), nextUp.getA().length);
			boolean [] B = Arrays.copyOf(nextUp.getB(), nextUp.getB().length);
			if(i < populationSize) {
				newData[i] = new DNA (A, B);
				++counter;
			}
		}
		for(int i = 0; i < survivors; ++i) {
			DNA nextUp = specimens.getDnaData()[index-i];
			boolean [] A = Arrays.copyOf(nextUp.getA(), nextUp.getA().length);
			boolean [] B = Arrays.copyOf(nextUp.getB(), nextUp.getB().length);
			if(i < populationSize) {
				newData[counter] = new DNA (A, B);
				newData[counter].mutate(mutationRate);
				++counter;
			}
		}
		while(counter < populationSize) {
			for(int i = 0; i < breedsPerTopGun; ++i) {
				if(counter < populationSize) {
					int toBreedIndex = index;
					while(toBreedIndex == index) {
						toBreedIndex = ConwayMain.getRandNum(populationSize);
					}
					Gamete a = getGameteA(specimens.getDnaData()[index]);
					Gamete b = getGameteB(specimens.getDnaData()[toBreedIndex]);

					DNA child = new DNA(a.getNewChromo(), b.getNewChromo());
					newData[counter] = child;
					++counter;
				}
			}
			--index;
			//--maxBreeds;
		}
		newSpecimens = new DNAData(newData);

	}

}
