package conwaysGameOfLifeSimulation.RunningConway;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

import conwaysGameOfLifeImplemented.ConwayRunImplementation;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import jssc.SerialPortException;
import main.ConwayMain;
import visualization.RunVis.ConwayVisualization;

public class ConwaySimulation {



	/*
	 *  Main method to begin and run simulation.
	 */
	
	/**
	 * Method to run implementation
	 * 
	 * Run Variables
	 * @param populationSize: Number of Games to be created.
	 * @param boardSize: Number of rows and columns
	 * @param chromosomeLength: Length of Seed
	 * @param generations: number of rounds of breeding
	 * @param chunkSize: Size of squares of genes
	 * @param runs: Number of runs in hardware
	 * 
	 * Record Keeping Variables
	 * @param printBoardEveryNGenerations: int to tell how often to print board
	 * @param printBoard: print board after a specified number of generations
	 * 
	 * Breeding Variables
	 * @param survivors: number of survivors between generations
	 * @param breedsPerTopGun: number of breeds for each of the breeding members of our population.
	 * 
	 * Visualization Variables
	 * @param screenSizeX: Horizontal pixels in screen, or desired window
	 * @param screenSizeY: Vertical pixels in screen or desired window
	 * @param sleepTime: Sleep time to instantiate board
	 * @param waitTime: Sleep time between iterations
	 * @param numberOfThreads: Number of threads available/aloted
	 * @param oscillatorDepth: The maximum number of steps an oscillator can be and our algorithm still catches it
	 * @param printFitness: Boolean to print the fitness of  each board in a generation
	 * @param checkedDepth: How far back we check steps. i.e. for a checked depth of 10, ten consecutive iterations
	 * would need to have the same changes for a specific number of steps looking back.
	 * @param printStats: Boolean to print the statistics at the end of each generation
	 * @param mutationRate: rate at which the gamete mutates.
	 * @throws FileNotFoundException
	 * @throws InterruptedException
	 * @throws SerialPortException
	 */
	public static void runSim(int populationSize, int boardSize, int chromosomeLength, int generations,
			int chunkSize, int runs, int printBoardEveryNGenerations, boolean printBoard, int survivors,
			int breedsPerTopGun, int screenSizeX, int screenSizeY, int sleepTime, int waitTime, int numberOfThreads,
			int oscillatorDepth, boolean printFitness, int checkedDepth, boolean printStats, double oddsOfLife,
			double mutationRate) throws FileNotFoundException, InterruptedException, SerialPortException {
		
		// Array to hold seed values for all games.
		DNA [] dna = new DNA[populationSize];
		// Creating our population.
		for(int i = 0; i < populationSize; ++i) {
			dna[i] = new DNA(chromosomeLength, oddsOfLife);
		}
		DNAData specimens = new DNAData (dna);
		ConwayData runner = new ConwayData(specimens, generations, boardSize);

		// Running our simulation on our population.
		runner.runConways(numberOfThreads, runs, oscillatorDepth, printBoard, printFitness, checkedDepth, chunkSize, 
				printBoardEveryNGenerations, printStats, survivors, breedsPerTopGun, mutationRate );
		ConwayMain.printLn("done running");
		ConwayMain.printToCsv(runner.getViabilities(), "game.csv");
		ConwayMain.printToCsv(runner.getTimes(), "times.csv");
		ConwayVisualization visual = new ConwayVisualization();
		visual.runVisualization(boardSize, screenSizeX, screenSizeY, runs, oscillatorDepth, checkedDepth, sleepTime, waitTime, runner.getBestBoard().getSeed());


	}

}
