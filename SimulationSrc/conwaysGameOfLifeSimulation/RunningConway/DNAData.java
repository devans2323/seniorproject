package conwaysGameOfLifeSimulation.RunningConway;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;

public class DNAData {
	
	private DNA[] dnaData;
	
	public DNAData(DNA [] dnaData){
		this.dnaData = dnaData;
	}
	
	public Seed[] toSeeds() {
		Seed[] seeds = new Seed[dnaData.length];
		for(int i = 0; i < dnaData.length; ++i) {
			seeds[i] = dnaData[i].toSeed();
		}
		return seeds;
	}
	
	public DNA[] getDnaData() {
		return dnaData;
	}
	
	public DNA getDNA(int i) {
		return dnaData[i];
	}
	
	public void setDNA(int i, DNA a) {
		dnaData[i] = a;
	}

}
