package conwaysGameOfLifeSimulation.RunningConway;

import java.io.FileNotFoundException;
import java.util.Arrays;

import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;

public class ConwayData {

	private DNAData specimens;
	private Seed [] seeds;
	private int generations;
	private int [][] viabilities;
	private int boardSize;
	private int populationSize;
	private int [][] deathsByOscillation;
	private int bestFoundViability = 0;
	private Conway bestBoard;
	private double [] times;

	/*
	 * Method to create a new ConwayData object.
	 * @param: specimens is the dna data of the initial generation.
	 * @param: generations is the number of generations.
	 * @param: boardSize is the X and Y length of our board and the 2D array that holds it.
	 */
	public ConwayData(DNAData specimens, int generations, int boardSize){
		this.specimens = specimens;
		this.seeds = specimens.toSeeds();
		this.generations = generations;
		this.viabilities = new int [generations][seeds.length];
		this.boardSize = boardSize;
		this.populationSize = seeds.length;
		this.times = new double [generations];
	}

	/*
	 * Method to get the Maximum breeds for the top seed.
	 * CURRENTALLY OUT OF USE.
	 * @return: the number of breeds for the top seed.
	 */
	@SuppressWarnings("unused")
	private int getMaxBreeds() {
		int a = 1;
		int b = 1;
		int c = -2*populationSize;
		int ans1 = (int)(-b + Math.sqrt(b - 4*a*c))/(2*a);
		int ans2 = (int)(-b - Math.sqrt(b - 4*a*c))/(2*a);
		return ans1 > 0 ? ans1+1 : ans2+1;
	}

	/*
	 * Method to return a thread for each of the processors with the boards evenly split between them.
	 * Currently work sharing is not implemented, but if balance becomes a problem it easily can be.
	 * @param: min the index of the first specimen to be run by this particular thread.
	 * @param: max one more than the index of the final specimen to be run by this thread.
	 * @param: generation is the number of the current generation.
	 * @param: oscillatorDepth is the max step oscillator that is checked for. A step is an iteration
	 * @param: checkedDepth is how many iterations before an oscillator is verified and the board is considered dead.
	 * @return: a thread set to run its portion of the boards.
	 */
	private Thread getThread (int min, int max, int threadNumber, int generation, int oscillatorDepth, int checkedDepth,
			boolean printFitness, int runs) {
		Thread r = new Thread(() ->  {
			for(int j = min; j < max; ++j) {
				Conway tester = new Conway(seeds[j], boardSize);
				tester.run(runs, oscillatorDepth, j, checkedDepth);
				viabilities [generation][j] = tester.getViability();
				if(tester.diedOfOscillation()) {
					++deathsByOscillation[generation][threadNumber];
				}
				if(printFitness) {
					ConwayMain.printLn(tester.getViability() + " Fitness " + j);
				}
				//changes[generation][j] = tester.getChangeArray();
			}
		});
		return r;
	}

	/*
	 * Method to run the Conway instances and determine their fitness numbers.
	 * @param: numberOfThreads is the number of processors that the work will be split between.
	 * @param: runs is the number of iterations each board will be given. This is a maximum value. Some boards will either oscillate or die out.
	 * @param: oscillatorDepthis the max step oscillator that is checked for. A step is an iteration.
	 * @param: printBoard boolean to determine if the best board should be printed each generation.
	 * @param: printFitness boolean to determine if the fitness of each board should be printed.
	 * @param: checkedDepth is how many iterations before an oscillator is verified and the board is considered dead.
	 * @param: chunkSize is the square root of the size of the gene. A gene is a square portion of a specimen.
	 * @param: printBoardEveryNGenerations is a boolean that prints the best board only once for every n generations, n being its value.
	 * @param: printStats is a boolean to choose whether or not to print the statistics of each generation.
	 * @param: breedsPerTopGun is the number of breeds for each breeding member of the population.
	 * @param: survivors is the number of boards passed directely to the next generation.
	 * @param: mutationRate is the rate of mutation in the gamete.
	 */
	public void runConways(int numberOfThreads, int runs, int oscillatorDepth, boolean printBoard, boolean printFitness, int checkedDepth,
			int chunkSize, int printBoardEveryNGenerations, boolean printStats, int breedsPerTopGun, int survivors,
			double mutationRate) throws FileNotFoundException {
		//this.changes = new int [generations][populationSize][runs];
		this.deathsByOscillation = new int[generations][numberOfThreads];
		Thread [] threads = new Thread[numberOfThreads];
		for(int j = 0; j < generations; ++j) {
			//One Generation
			long Start;
			ConwayMain.printLn(" Parallel Start.");
			Start = System.currentTimeMillis();
			int step = populationSize/threads.length;
			int min = 0;
			int max = step;
			for(int i = 0; i < numberOfThreads; ++i) {
				final int min_ = min;
				final int max_ = max;
				threads[i] = getThread(min_, max_, i, j, oscillatorDepth, checkedDepth, printFitness, runs);
				threads[i].start();
				min = min + step;
				if(i != threads.length - 2) {
					max = max + step;
				}
				else {
					max = populationSize;
				}
			}
			for(int i = 0; i < threads.length; ++i) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			int [] viabilitiesPresort = Arrays.copyOf(viabilities[j], viabilities[j].length);
			Arrays.parallelSort(viabilities[j]);
			int bestViability = viabilities[j][populationSize - 1];
			ConwayMain.printLn("The best viability: " + bestViability + " Gen: " + (j + 1));
			if(bestViability > bestFoundViability) {
				for(int i = 0; i < populationSize; ++i) {
					if(viabilitiesPresort[i] == viabilities[j][populationSize - 1]) {
						Conway test = new Conway(seeds[i], boardSize);
						bestBoard = test;
						break;
					}
				}
				bestFoundViability = bestViability;
			}
			if(printBoard && j % printBoardEveryNGenerations == 0) {
				for(int i = 0; i < populationSize; ++i) {
					if(viabilitiesPresort[i] == viabilities[j][populationSize - 1]) {
						Conway test = new Conway(seeds[i], boardSize);
						test.printBoard();
						break;
					}
				}
			}
			double time = (System.currentTimeMillis() - Start)/1000.0;
			times[j] = time;
			if(printStats) {
				printStats(j, runs, time);
			}
			//End of Generation
			Breeder getNext = new Breeder(specimens, chunkSize, boardSize, viabilitiesPresort, viabilities[j], mutationRate);
			getNext.Breed(survivors, breedsPerTopGun);
			specimens = getNext.getNewSpecimens();
			seeds = specimens.toSeeds();

		}
		bestBoard.csvOutput("bestBoard.csv");
	}

	/*
	 * Method to print the statistics of a specific generation.
	 * @param: generation is the generation to be looked at.
	 * @param: runs is the number of iterations for each board in that generation.
	 * @param: Start is the start time of that generation. This is used for timing.
	 */
	private void printStats(int generation, int runs, double time) {
		int [] test = viabilities[generation];
		int hold = 0;
		int maxes = 0;
		for(int i = 0; i < test.length; ++i) {
			hold += test[i];
			if(test[i] >= runs) {
				++maxes;
			}
		}
		double toCompletion = Math.round((maxes*10000.0)/(populationSize*1.0))/100.0;
		double avg = hold*1.0/populationSize;
		double holder = 0;
		for(int i = 0; i < test.length; ++i) {
			holder += (test[i] - avg)*(test[i] - avg);
		}
		double standardDeviation = Math.sqrt(holder/populationSize);
		ConwayMain.printLn("Generation: " + (generation + 1));
		ConwayMain.printLn("Parallel Done. " + time + " Seconds");
		ConwayMain.printLn("Average: " + Math.round(avg));
		ConwayMain.printLn("Standard Deviation: " + Math.round(standardDeviation*100)/100.0);
		ConwayMain.printLn(toCompletion + "% are running to completion.");
		ConwayMain.printLn(getDeathsByOcsillationSingleGeneration(generation)*100.0/populationSize*1.0
				+ "% deaths by oscillation");
		ConwayMain.printLn("");
		ConwayMain.printLn("");
	}

	/*
	 * Method to get the number of deaths by oscillation.
	 * @return: returns an int array with each index holding the number of oscillation deaths per generation.
	 */
	public int [] getDeathsByOcsillationArray() {
		int [] deathsPerGeneration = new int [generations];
		for (int j = 0; j < generations; ++j) {
			for(int i = 0; i < deathsByOscillation[0].length; ++i) {
				deathsPerGeneration[j] += deathsByOscillation[j][i];
			}
		}
		return deathsPerGeneration;
	}

	/*
	 * Method to get the deaths by oscillation of a single generation.
	 * @param: generation is the number of the generation to be looked at.
	 * @return: an int that represents the number of deaths by oscillation in the specified generation.
	 */
	public int getDeathsByOcsillationSingleGeneration(int generation) {
		int hold = 0;
		for(int i = 0; i < deathsByOscillation[0].length; ++i) {
			hold += deathsByOscillation[generation][i];
		}
		return hold;
	}

	/*
	 * Method to get the changes of every iteration, board, and generation.
	 * @return: a 3D array that is indexed [generation][specimen][iteration].
	 */
	//	public int [][][] getChanges(){
	//		return this.changes;
	//	}

	/*
	 * Method to get the fitness of each specimen in each generation.
	 * @return: 2D array that is indexed [generation][specimen]
	 */
	public int [][] getViabilities(){
		return viabilities;
	}

	/*
	 * Method to get the number of generations.
	 * @return: the number of generations.
	 */
	public int getGenerations() {
		return generations;
	}

	/*
	 * Method to set the number of generations. Method could be used to compare varying the 
	 * amount of generations on a specific population.
	 * @param: the new number of generations desired.
	 */
	public void setGeneration(int generations) {
		this.generations = generations;
	}
	
	/*
	 * Method to get the times for each of the generations.
	 * @return: an array of doubles with each generation's time to run.
	 */
	public double [] getTimes() {
		return this.times;
	}
	
	/*
	 * Method to get the best Board
	 * @return: returns the best board from the previous generation.
	 */
	public Conway getBestBoard() {
		return this.bestBoard;
	}
}
