package testing.ConwayPrototyping;

import conwaysGameOfLifeImplemented.ConwayRunImplementation;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;

public class TestingConway {
	private static boolean [] intToBool(int [] a) {
		boolean [] b = new boolean [a.length];
		for(int i = 0; i < a.length; ++i) {
			if(a[i] == 1) {
				b[i] = true;
			}
			else {
				b[i] = false;
			}
		}
		return b;
	}

	public static void main(String[] args) {
		int seedLength = 66000;
		int boardSize = 256;
		int [] tes = new int [seedLength];
		for(int i = 0; i < seedLength; ++i) {
			if(ConwayMain.randomBool()) {
				tes[i] = 1;
			}
			else {
				tes[i] = 0;
			}
		}
		boolean [] te = intToBool(tes);
		Seed test = new Seed(te);
		ConwayProto tester = new ConwayProto(test, boardSize, 10, 0, 10);
		tester.run(1000);

		System.out.println(tester.getViability() + " Fitness");

	}

}
