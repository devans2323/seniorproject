package testing.ConwayDataPrototyping;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;

public class TestingConwayData {

	public static void main(String[] args) throws InterruptedException {
		int numSeeds = 1000; 
		int seedLength = 256;
		int boardSize = 16;
		int runs = 100;
		int oscillatorDepth = 10;
		int checkedDepth = 10;
		int numberOfThreads = 4;
		
		boolean printBoard = false;
		boolean printFitness = true;
		
		Seed [] seeds = new Seed[numSeeds];
		for(int i = 0; i < numSeeds; ++i) {
			seeds[i] = new Seed(seedLength);
		}
		ConwayDataProto tester = new ConwayDataProto(seeds, boardSize, runs, oscillatorDepth, printBoard, printFitness, checkedDepth);
		long Start;
		System.out.println(" Parallel Start.");
		Start = System.currentTimeMillis();
		tester.runParallelConways(numberOfThreads);
		int [] test = tester.getViabilities();
		int hold = 0;
		int maxes = 0;
		for(int i = 0; i < test.length; ++i) {
			hold += test[i];
			if(test[i] >= runs) {
				++maxes;
			}
		}
		double toCompletion = Math.round((maxes*10000.0)/(numSeeds*1.0))/100.0;
		double avg = hold*1.0/numSeeds;
		double holder = 0;
		for(int i = 0; i < test.length; ++i) {
			holder += (test[i] - avg)*(test[i] - avg);
		}
		double standardDeviation = Math.sqrt(holder/numSeeds);
		
		System.out.println("Parallel Done. " + (System.currentTimeMillis() - Start)/1000.0 + " Seconds");
		System.out.println("Average: " + Math.round(avg));
		System.out.println("Standard Deviation: " + Math.round(standardDeviation*100)/100.0);
		System.out.println(toCompletion + "% are running to completion.");
		System.out.println(tester.getDeathsByOcsillation()*100.0/numSeeds*1.0 + "% deaths by oscillation");
//		
//		System.out.println();
//		
//		System.out.print("Sequential Start.");
//		Start = System.currentTimeMillis();
//		tester.runSequentialConways();
//		test = tester.getViabilities();
//		hold = 0;
//		maxes = 0;
//		for(int i = 0; i < test.length; ++i) {
//			hold += test[i];
//			if(test[i] >= runs) {
//				++maxes;
//			}
//		}
//		toCompletion = Math.round((maxes*10000.0)/(numSeeds*1.0))/100.0;
//		avg = hold*1.0/numSeeds;
//		holder = 0;
//		for(int i = 0; i < test.length; ++i) {
//			holder += (test[i] - avg)*(test[i] - avg);
//		}
//		standardDeviation = Math.sqrt(holder/numSeeds);
//		
//		System.out.println("Parallel Done." + (System.currentTimeMillis() - Start));
//		System.out.println("Average: " + Math.round(avg));
//		System.out.println("Standard Deviation: " + Math.round(standardDeviation*100)/100.0);
//		System.out.println(toCompletion + "% are running to completion.");
	}

}
