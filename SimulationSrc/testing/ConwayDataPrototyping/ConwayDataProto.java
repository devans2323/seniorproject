package testing.ConwayDataPrototyping;

import java.util.Arrays;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;
import testing.ConwayPrototyping.ConwayProto;

public class ConwayDataProto {

	private Seed [] seeds;
	private int [] viabilities;
	private int boardSize;
	private int populationSize;
	private int runs;
	private int[][] changes;
	private boolean printBoard;
	private boolean printFitness;
	private int oscillatorDepth;
	private int checkedDepth;
	private int [] deathsByOscillation;

	ConwayDataProto(Seed [] seeds, int boardSize, int runs, int oscillatorDepth, boolean printBoard, boolean printFitness, int checkedDepth){
		this.seeds = seeds;
		this.viabilities = new int[seeds.length];
		this.boardSize = boardSize;
		this.populationSize = seeds.length;
		this.runs = runs;
		this.changes = new int [populationSize][runs];
		this.printBoard = printBoard;
		this.printFitness = printFitness;
		this.oscillatorDepth = oscillatorDepth;
		this.checkedDepth = checkedDepth;
	}

//	public void runSequentialConways() throws InterruptedException {
//		for(int i = 0; i < populationSize; ++i) {
//			Conway tester = new Conway(seeds[i], boardSize, oscillatorDepth, i, checkedDepth);
//			tester.run(runs);
//			viabilities [i] = tester.getViability();
//			if(printFitness) {
//				System.out.println(tester.getViability() + " Fitness " + i);
//			}
//			changes[i] = tester.getChangeArray();
//		}
//		int [] viabilitiesPresort = Arrays.copyOf(viabilities, viabilities.length);
//		Arrays.parallelSort(viabilities);
//		System.out.println("The best viability: " + viabilities[populationSize-1]);
//		if(printBoard) {
//			for(int i = 0; i < populationSize; ++i) {
//				if(viabilitiesPresort[i] == viabilities[populationSize - 1]) {
//					Conway test = new Conway(seeds[i], boardSize, oscillatorDepth, i, checkedDepth);
//					test.printBoard();
//					System.out.println("found at " + i);
//					break;
//				}
//			}
//		}
//	}
	
	private Thread getThread(int holders, int maxss, int id) {
		Thread r = new Thread(() -> {
			for(int j = holders; j < maxss; ++j) {
				ConwayProto tester = new ConwayProto(seeds[j], boardSize, oscillatorDepth, j, checkedDepth);
				tester.run(runs);
				viabilities [j] = tester.getViability();
				if(tester.diedOfOscillation()) {
					++deathsByOscillation[id];
				}
				if(printFitness) {
					ConwayMain.printLn(tester.getViability() + " Fitness " + j);
				}
				changes[j] = tester.getChangeArray();
			}
		});
		return r;
	}

	public void runParallelConways(int numberOfThreads) throws InterruptedException {
		//		Thread [] threads = new Thread[populationSize];
		//		for(int i = 0; i < populationSize; ++i) {
		//			Conway tester = new Conway(seeds[i], boardSize);
		//			final int ii = i;
		//			threads[i] = new Thread(() -> {
		//				tester.run(runs);
		//				viabilities [ii] = tester.getViability();
		//				System.out.println(tester.getViability() + " Fitness " + ii);
		//				changes[ii] = tester.getChangeArray();
		//			});
		//			threads[i].start();
		//		}
		deathsByOscillation = new int [numberOfThreads];
		Thread [] threads = new Thread[numberOfThreads];
		int step = populationSize/threads.length;
		int holder = 0;
		int maxs = step;
		for(int i = 0; i < numberOfThreads; ++i) {
			final int holders = holder;
			final int maxss = maxs;
			threads[i] = getThread(holders, maxss, i);
			threads[i].start();
			holder = holder + step;
			if(i != threads.length - 2) {
				maxs = maxs + step;
			}
			else {
				maxs = populationSize;
			}
		}
		for(int i = 0; i < threads.length; ++i) {
			threads[i].join();
		}
		int [] viabilitiesPresort = Arrays.copyOf(viabilities, viabilities.length);
		Arrays.parallelSort(viabilities);
		ConwayMain.printLn("The best viability: " + viabilities[populationSize-1]);
		if(printBoard) {
			for(int i = 0; i < populationSize; ++i) {
				if(viabilitiesPresort[i] == viabilities[populationSize - 1]) {
					ConwayProto test = new ConwayProto(seeds[i], boardSize, oscillatorDepth, i, checkedDepth);
					test.printBoard();
					System.out.println("found at " + i);
					break;
				}
			}
		}
	}

	public int [][] getChanges(){
		return this.changes;
	}

	public int [] getViabilities(){
		return viabilities;
	}
	 public int getDeathsByOcsillation() {
		 int hold = 0;
		 for(int i = 0; i < deathsByOscillation.length; ++i) {
			 hold += deathsByOscillation[i];
		 }
		 return hold;
	 }
}
