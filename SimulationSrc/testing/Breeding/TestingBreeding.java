package testing.Breeding;

import conwaysGameOfLifeSimulation.Breeding.Gamete;
import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;

public class TestingBreeding {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		int boardSize = 32;
		int chunkSize = 4;
		int chromoLength = 1024;
		double mutationRate = .001;
		Seed testSeedA = new Seed(chromoLength);
		Seed testSeedB = new Seed(chromoLength);
		DNA testDNA = new DNA(testSeedA.getSeed(), testSeedB.getSeed());
		Gamete test = new Gamete(testDNA, chromoLength, boardSize, chunkSize, mutationRate);
		Conway testA = new Conway(testSeedA, boardSize);
		Conway testB = new Conway(testSeedB, boardSize);
		Seed newChromo = new Seed(test.getNewChromo());
		Conway bred = new Conway(newChromo, boardSize);
		System.out.println("A");
		//testA.printBoard();
		testSeedA.printSeed();
		System.out.println("B");
		//testB.printBoard();
		testSeedB.printSeed();
		System.out.println("Final");
		//bred.printBoard();
		newChromo.printSeed();
	}

}
