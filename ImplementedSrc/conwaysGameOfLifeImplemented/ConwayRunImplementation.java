package conwaysGameOfLifeImplemented;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import main.ConwayMain;
import conwaysGameOfLifeSimulation.RunningConway.DNAData;
import jssc.SerialPortException;
import visualization.RunVis.ConwayVisualization;

public class ConwayRunImplementation {

	/**
	 * Method to run implementation
	 * 
	 * Run Variables
	 * @param populationSize: Number of Games to be created.
	 * @param boardSize: Number of rows and columns
	 * @param chromosomeLength: Length of Seed
	 * @param generations: number of rounds of breeding
	 * @param chunkSize: Size of squares of genes
	 * @param runs: Number of runs in hardware
	 * @param oddsOfLife: odds that a cell in an initial board will be alive.
	 * 
	 * Record Keeping Variables
	 * @param printBoardEveryNIterations: int to tell how often to print board
	 * @param printBoard: print board after a specified number of generations
	 * @param saveDataFile: how often the data is written
	 * 
	 * I/O Variables
	 * @param bytesToRead: bytes sent back to determine fitness
	 * 
	 * Breeding Variables
	 * @param survivors: number of survivors between generations
	 * @param breedsPerTopGun: number of breeds for each of the breeding members of our population.
	 * @param mutationRate: odds of the gamete mutating.
	 * 
	 * Visualization Variables
	 * @param screenSizeX: Horizontal pixels in screen, or desired window
	 * @param screenSizeY: Vertical pixels in screen or desired window
	 * @param sleepTime: Sleep time to instantiate board
	 * @param waitTime: Sleep time between iterations 
	 * @throws FileNotFoundException
	 * @throws SerialPortException
	 * @throws InterruptedException
	 */
	
	public static void runImp(int populationSize, int boardSize, int chromosomeLength, int generations,
			int chunkSize, int runs, int printBoardEveryNIterations, boolean printBoard, int bytesToRead,
			int survivors, int breedsPerTopGun, int screenSizeX, int screenSizeY, int sleepTime, int waitTime,
			boolean runVis, double oddsOfLife, double mutationRate, String [] ports, int saveDataFile)
					throws FileNotFoundException, SerialPortException, InterruptedException {
		// Array to hold seed values for all games.
		DNA [] dna = new DNA[populationSize];

		// Array to hold the fitness scores for all games
		int [][] fitness;

		for(int i = 0; i < populationSize; ++i) {
			dna[i] = new DNA(chromosomeLength, oddsOfLife);
		}
		DNAData specimens = new DNAData (dna);
		ConwayDataImplementation runner = new ConwayDataImplementation(specimens);
		runner.runConways(bytesToRead, generations, boardSize, chunkSize, printBoard, printBoardEveryNIterations, survivors, breedsPerTopGun,
				mutationRate, ports, saveDataFile);
		fitness = runner.getViabilities();
		ConwayMain.printToCsv(fitness, "GAData/ImpGame.csv");
		ConwayMain.printToCsv(runner.getTimes(), "GAData/ImpTimes.csv");
		if(runVis) {
			ConwayVisualization visual = new ConwayVisualization();
			visual.runVisualization(boardSize, screenSizeX, screenSizeY, runs, 1, 10, sleepTime, waitTime, runner.getBestBoard().getSeed());
		}
	}

}
