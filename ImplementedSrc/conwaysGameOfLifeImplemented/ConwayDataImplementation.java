package conwaysGameOfLifeImplemented;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import conwaysGameOfLifeSimulation.RunningConway.Breeder;
import conwaysGameOfLifeSimulation.RunningConway.DNAData;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;
import main.ConwayMain;

public class ConwayDataImplementation {

	private DNAData specimens;
	private Seed[] seeds;
	private int [][] fitness;
	private int populationSize;
	private ConwayImplementation bestBoard;
	private double [] times;

	ConwayDataImplementation(DNAData specimens){
		this.specimens = specimens;
		this.seeds = specimens.toSeeds();
		this.populationSize = seeds.length;
	}

	private Thread getThread(int min, int max, int bytesToRead, int generation, int boardSize, SerialPort port) {
		Thread a = new Thread (()-> {
			ConwayImplementation game1;
			for(int i = min; i < max; ++i) {
				game1 = new ConwayImplementation(seeds[i], boardSize);
				try {
					game1.runWrite(port);
					game1.runRead(port, bytesToRead);
					fitness [generation][i] = game1.getFitness();
				} catch (SerialPortException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		return a;
	}

	private Thread getThread2BoardStart(int min, int max, int bytesToRead, int generation, int boardSize, SerialPort port) {

		Thread a = new Thread (()-> {
			ConwayImplementation game1 = new ConwayImplementation(seeds[min], boardSize);
			ConwayImplementation game2;
			if(max - min > 1) {
				game2 = new ConwayImplementation(seeds[min+1], boardSize);
			}else {
				game2 = new ConwayImplementation(seeds[min], boardSize);
			}
			try {
				game1.runWrite(port);
			} catch (SerialPortException e1) {
				e1.printStackTrace();
			}
			for(int i = min+1; i < max; ++i) {
				boolean game2New = (i - min) % 2 == 1;
				try {
					if(game2New) {
						game2 = new ConwayImplementation(seeds[i], boardSize);
						game2.runWrite(port);
						game1.runRead(port, bytesToRead);
						fitness [generation][i-1] = game1.getFitness();
					}else {
						game1 = new ConwayImplementation(seeds[i], boardSize);
						game1.runWrite(port);
						game2.runRead(port, bytesToRead);
						fitness [generation][i-1] = game2.getFitness();
					}
				} catch (SerialPortException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		return a;
	}
	
	private void saveData() throws FileNotFoundException {
		ConwayMain.printToCsv(fitness, "GAData/ImpGame.csv");
		ConwayMain.printToCsv(times, "GAData/ImpTimes.csv");
		bestBoard.csvOutput("GAData/bestBoard.csv");
	}

	public void runConways(int bytesToRead, int generations, int boardSize, int chunkSize, boolean printBoard, int printBoardEveryNIterations, 
			int survivors, int breedsPerTopGun, double mutationRate, String [] ports, int saveDataFile ) throws InterruptedException, SerialPortException, FileNotFoundException  {
		int bestOverallFitness = 0;
		times = new double [generations];
		String[] portNames = SerialPortList.getPortNames();
		int numberOfPorts = portNames.length;
		int counter = 0;

		for(int i = 0; i < numberOfPorts; ++i) {
			for(int j = 0; j < ports.length; ++j) {
				if(portNames[i].equals(ports[j])) {
					++counter;
					System.out.println("port " + counter);
				}
			}
		}

		SerialPort[] uartPorts = new SerialPort[counter];
		counter = 0;
		for(int i = 0; i < numberOfPorts; ++i) {
			for(int j = 0; j < ports.length; ++j) {
				if(portNames[i].equals(ports[j])) {
					uartPorts[counter] = new SerialPort(ports[j]);
					uartPorts[counter].openPort();
					++counter;
				}
			}
		}
		numberOfPorts = counter;
		int chunks = populationSize/(numberOfPorts);
		

		this.fitness = new int [generations][populationSize];
		for(int j =  0; j < generations; ++j) {
			long Start = System.currentTimeMillis();
			Thread[] threads = new Thread[numberOfPorts];
			int min = 0;
			int max = chunks;
			for(int i = 0; i < numberOfPorts; ++i) {
				if(i != numberOfPorts - 1) {
					threads[i] = getThread(min, max, bytesToRead, j, boardSize, uartPorts[i]);
					min = min + chunks;
					max = max + chunks;
				}
				else {
					threads[i] = getThread(min, populationSize, bytesToRead, j, boardSize, uartPorts[i]);
					min = min + chunks;
					max = max + chunks;
				}
			}

			for(int i = 0; i < numberOfPorts; ++i) {
				threads[i].start();
			}
			for(int i = 0; i < numberOfPorts; ++i) {
				threads[i].join();
			}


			int [] viabilitiesPresort = Arrays.copyOf(fitness[j], fitness[j].length);
			Arrays.sort(fitness[j]);
			int bestFitness = fitness[j][populationSize - 1];
			if(bestFitness > bestOverallFitness) {
				for(int i = 0; i < populationSize; ++i) {
					if(viabilitiesPresort[i] == fitness[j][populationSize - 1]) {
						ConwayImplementation test = new ConwayImplementation(seeds[i], boardSize);
						bestBoard = new ConwayImplementation(new Seed(Arrays.copyOf(test.getSeed().getSeed(), test.getSeed().getSeed().length)), boardSize);
						break;
					}
				}
				bestOverallFitness = bestFitness;
			}
			long breedStart = System.currentTimeMillis();
			Breeder getNext = new Breeder(specimens, chunkSize, boardSize, viabilitiesPresort, fitness[j], mutationRate);
			getNext.Breed(survivors, breedsPerTopGun);
			specimens = getNext.getNewSpecimens();
			seeds = specimens.toSeeds();
			long breedingTime = System.currentTimeMillis() - breedStart;

			double time = (System.currentTimeMillis() - Start)/1000.0;
			times[j] = time;
			printStats(j, 8000, time);
			System.out.println("Finished Gen: " + (j+1) + " Breeding time: " + breedingTime);
			
			if(j % saveDataFile == 0) {
				saveData();
				ConwayMain.printOutput();
			}
		}
		try {
			bestBoard.csvOutput("GAData/bestBoard.csv");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i = 0; i < numberOfPorts; ++i) {
			uartPorts[i].closePort();
		}
	}

	private void printStats(int generation, int runs, double time) {
		int [] test = fitness[generation];
		int hold = 0;
		int maxes = 0;
		for(int i = 0; i < test.length; ++i) {
			hold += test[i];
			if(test[i] >= runs) {
				++maxes;
			}
		}
		double toCompletion = Math.round((maxes*10000.0)/(populationSize*1.0))/100.0;
		double avg = hold*1.0/populationSize;
		double holder = 0;
		for(int i = 0; i < test.length; ++i) {
			holder += (test[i] - avg)*(test[i] - avg);
		}
		double standardDeviation = Math.sqrt(holder/populationSize);
		ConwayMain.printLn("Generation: " + (generation + 1));
		ConwayMain.printLn("Parallel Done. " + time + " Seconds");
		ConwayMain.printLn("Average: " + Math.round(avg));
		ConwayMain.printLn("Standard Deviation: " + Math.round(standardDeviation*100)/100.0);
		ConwayMain.printLn(toCompletion + "% are running to completion.");
		ConwayMain.printLn("");
		ConwayMain.printLn("");
	}

	public int [][] getViabilities(){
		return fitness;
	}

	/*
	 * Method to get the times for each of the generations.
	 * @return: an array of doubles with each generation's time to run.
	 */
	public double [] getTimes() {
		return this.times;
	}

	/*
	 * Method to get the best Board
	 * @return: returns the best board from the previous generation.
	 */
	public ConwayImplementation getBestBoard() {
		return this.bestBoard;
	}
}
