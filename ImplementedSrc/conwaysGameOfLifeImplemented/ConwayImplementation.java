package conwaysGameOfLifeImplemented;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class ConwayImplementation {
	private Seed seed;
	private int boardSize;
	private int fitness;

	/*
	 * Constructor
	 * @param: seed is the Seed the game is built off of.
	 * @param: size is the number of rows and columns.
	 */
	public ConwayImplementation(Seed seed, int size){
		this.seed = seed;
		this.boardSize = size;
		this.fitness = 0;
	}
	
	/*
	 * Method to print board.
	 */
	public void printBoard() {
		int k = 0; 
		for(int i = 0; i < boardSize; ++i) {
			for (int j = 0; j < boardSize; ++j) {
				if(seed.getSeed()[k]) {
					ConwayMain.print("1 ");
				}else {
					ConwayMain.print("0 ");
				}
				++k;
			}
			ConwayMain.printLn("");
		}
	}
	
	/*
	 * Method to print board to a csv output file.
	 */
	public void csvOutput(String name) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Data_Files/" + name));
		StringBuilder sb = new StringBuilder();
		int k = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if(seed.getSeed()[k]) {
					sb.append("1");
				}
				else {
					sb.append("0");
				}
				k++;
				if(j != boardSize - 1) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		pw.write(sb.toString());
		pw.close();
	}

	/*
	 * Method to get the initialization seed.
	 * return: Seed that the instance was built off of.
	 */
	public Seed getSeed() {
		return this.seed;
	}
	
	/*
	 * Method to get the number of rows and columns in the board.
	 * @return: number of rows and columns in the board.
	 */
	public int getSize() {
		return this.boardSize;
	}

	/*
	 * Method to return the viability of the last run.
	 * @return: returns the number of iterations until 
	 * 0 changes or steady state from the last time run was called.
	 */
	public int getFitness() {
		return this.fitness;
	}
	
	// https://stackoverflow.com/questions/26944282/java-boolean-to-byte-and-back
	public static byte[] toBytes(boolean[] input) {
		byte[] toReturn = new byte[input.length / 8];
		for (int entry = 0; entry < toReturn.length; entry++) {
			for (int bit = 0; bit < 8; bit++) {
				if (input[entry * 8 + bit]) {
					toReturn[entry] |= (128 >> bit);
				}
			}
		}

		return toReturn;
	}

	/*
	* The run method. This method runs the board through n iterations of Conway.
	* The loop breaks once the map stops changing, or reaches a constant steady
	* state.
	* @param: n is the number of iterations to run.
	*/
	public void runWrite(SerialPort a) throws SerialPortException {
		a.setParams(2000000000, 8, 1, 0);
		byte[] send = toBytes(seed.getSeed());
		a.writeBytes(send);
		
	}
	
	public void runRead(SerialPort a, int bytesToRead) throws SerialPortException {
		int[] aa = a.readIntArray(bytesToRead);
		int fitness = 0;
		fitness = fitness | aa[0];
		fitness = fitness | ((aa[1] << 8) | (fitness));
		fitness = fitness | ((aa[2] << 16) | (fitness));
		fitness = fitness | ((aa[3] << 24) | (fitness));
		this.fitness = fitness;
	}

}
