package portTesting;

import java.io.*;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.*;



public class USBPorting {

	// https://stackoverflow.com/questions/26944282/java-boolean-to-byte-and-back
	public static byte[] toBytes(boolean[] input) {
		byte[] toReturn = new byte[input.length / 8];
		for (int entry = 0; entry < toReturn.length; entry++) {
			for (int bit = 0; bit < 8; bit++) {
				if (input[entry * 8 + bit]) {
					toReturn[entry] |= (128 >> bit);
				}
			}
		}

		return toReturn;
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {
		String[] portNames = SerialPortList.getPortNames();

		if (portNames.length == 0) {
			System.out.println("There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
			System.out.println("Press Enter to exit...");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		for(int i = 0; i < portNames.length; ++i) {
			System.out.println(portNames[i]);
		}
//		SerialPort a = new SerialPort(portNames[0]);
//		a.openPort();
//		a.setParams(9600, 8, 1, 0);
//		Seed seedy = new Seed(100);
//		boolean [] toSend = seedy.getSeed();
//		byte [] send = toBytes(toSend);
//		a.writeBytes(send);
//		byte[] aa = a.readBytes(toSend.length/8);
//		seedy.printSeed();
//		for(int i = 0; i < aa.length; ++i) {
//			Integer print = new Integer(aa[i]);
//			System.out.println(print.toBinaryString(print.intValue()));
//		}
	}
}
