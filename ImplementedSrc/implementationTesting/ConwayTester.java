package implementationTesting;


import conwaysGameOfLifeImplemented.ConwayImplementation;
import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class ConwayTester {

	public static void main(String[] args) throws SerialPortException {
		String[] portNames = SerialPortList.getPortNames();
		SerialPort a = new SerialPort(portNames[0]);
		a.openPort();
		a.setParams(2000000000, 8, 1, 0);

		SerialPort b = new SerialPort(portNames[1]);
		b.openPort();
		b.setParams(2000000000, 8, 1, 0);

		SerialPort c = new SerialPort(portNames[2]);
		c.openPort();
		c.setParams(2000000000, 8, 1, 0);

		SerialPort [] ports = { a, b, c };

		for(int j = 0; j < 1; ++j) {
			Seed testSeed = new Seed(256*256);
			Seed testSeed2 = new Seed(256*256);
//			Seed testSeed3 = new Seed(256*256);
//			Seed testSeed4 = new Seed(256*256);
//			Seed testSeed5 = new Seed(256*256);
//			Conway testS = new Conway(testSeed, 256);
//			Conway testS2 = new Conway(testSeed2, 256);
////			Conway testS3 = new Conway(testSeed3, 256);
////			Conway testS4 = new Conway(testSeed4, 256);
////			Conway testS5 = new Conway(testSeed5, 256);
//			testS.run(16384, 1, 0, 10);
//			testS2.run(16384, 1, 0, 10);
//			testS3.run(8000, 1, 0, 10);
//			testS4.run(8000, 1, 0, 10);
//			testS5.run(8000, 1, 0, 10);
//			System.out.println("ours for test " + 1 +": " + testS.getViability());
//			System.out.println("ours for test " + 2 +": " + testS2.getViability());
//			System.out.println("ours for test " + 3 +": " + testS3.getViability());
//			System.out.println("ours for test " + 4 +": " + testS4.getViability());
//			System.out.println("ours for test " + 5 +": " + testS5.getViability());
			//		
			for(int i = 0; i < ports.length; ++i) {
				ConwayImplementation test = new ConwayImplementation(testSeed, 256);
				ConwayImplementation test2 = new ConwayImplementation(testSeed2, 256);
//				ConwayImplementation test3 = new ConwayImplementation(testSeed3, 256);
//				ConwayImplementation test4 = new ConwayImplementation(testSeed4, 256);
//				ConwayImplementation test5 = new ConwayImplementation(testSeed5, 256);

				long start = System.currentTimeMillis();
				//				test.runWrite(ports[i]);
				//				test2.runWrite(ports[i]);
				//				test.runRead(ports[i], 4);
				//				test3.runWrite(ports[i]);
				//				test2.runRead(ports[i], 4);
				//				test4.runWrite(ports[i]);
				//				test3.runRead(ports[i], 4);
				//				test5.runWrite(ports[i]);
				//				test4.runRead(ports[i], 4);
				//				test5.runRead(ports[i], 4);
				test.runWrite(ports[i]);
				test.runRead(ports[i], 4);
				test2.runWrite(ports[i]);
				test2.runRead(ports[i], 4);
//				test3.runWrite(ports[i]);
//				test3.runRead(ports[i], 4);
//				test4.runWrite(ports[i]);
//				test4.runRead(ports[i], 4);
//				test5.runWrite(ports[i]);
//				test5.runRead(ports[i], 4);
				System.out.println("Total Time 2 boards at start: "+ i + " iteration " + (System.currentTimeMillis() - start));

				int one = test.getFitness();
				int two = test2.getFitness();
//				int three = test3.getFitness();
//				int four = test4.getFitness();
//				int five = test5.getFitness();


//				start = System.currentTimeMillis();
//				test.runWrite(ports[i]);
//				test.runRead(ports[i], 4);
//				test2.runWrite(ports[i]);
//				test2.runRead(ports[i], 4);
//				test3.runWrite(ports[i]);
//				test3.runRead(ports[i], 4);
//				test4.runWrite(ports[i]);
//				test4.runRead(ports[i], 4);
//				test5.runWrite(ports[i]);
//				test5.runRead(ports[i], 4);
//				//System.out.println("Total Time 1 board at start: " + (System.currentTimeMillis() - start));
//
//				int one1 = test.getFitness();
//				int two1 = test2.getFitness();
//				int three1 = test3.getFitness();
//				int four1 = test4.getFitness();
//				int five1 = test5.getFitness();
				System.out.println("Seed 1 " + one);
				System.out.println("Seed 2 " + two);
//				System.out.println("Seed 3 " + three);
//				System.out.println("Seed 4 " + four);
//				System.out.println("Seed 5 " + five);
//				if(one != one1 || two != two1 || three != three1 || four != four1 || five != five1) {
//					System.out.println("ERROR!!!!!!!!");
//					System.out.println("Seed 1 " + one);
//					System.out.println("Seed 2 " + two);
//					System.out.println("Seed 3 " + three);
//					System.out.println("Seed 4 " + four);
//					System.out.println("Seed 5 " + five);
//					System.out.println();
//					System.out.println("1 Board");
//					System.out.println("Seed 1 " + one1);
//					System.out.println("Seed 2 " + two1);
//					System.out.println("Seed 3 " + three1);
//					System.out.println("Seed 4 " + four1);
//					System.out.println("Seed 5 " + five1);
//				}

			}
		}
		//b.closePort();
		a.closePort();
	}
}
