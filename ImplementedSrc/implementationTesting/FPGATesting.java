package implementationTesting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.BitSet;

import conwaysGameOfLifeImplemented.ConwayImplementation;
import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;
import visualization.RunVis.ConwayVisualization;

public class FPGATesting {

	private static StringBuilder output = new StringBuilder();


	public synchronized static void printLn(String a) {
		output.append(a + "\n");
	}

	/*
	 * Method to print to the console.
	 * @param: a is the string to be printed.
	 */
	public synchronized static void print(String a) {
		output.append(a);
	}

	//stackoverflow
	static boolean[] toBooleanArray(byte[] bytes) {
		BitSet bits = BitSet.valueOf(bytes);
		boolean[] bools = new boolean[bytes.length * 8];
		for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i+1)) {
			bools[i] = true;
		}
		return bools;
	}

	public static void main(String[] args) throws Exception  {
		String[] portNames = SerialPortList.getPortNames();
		SerialPort [] a = new SerialPort[portNames.length];
		for(int i = 0; i < portNames.length; ++i) {
			a[i] = new SerialPort(portNames[i]);
			a[i].openPort();
			a[i].setParams(1504000, 8, 1, 0);
		}

		Thread [] threads = new Thread[portNames.length];
		long Starter = System.currentTimeMillis();
		int boardsPerThread = 1;
		Seed [] testSeeds = new Seed[boardsPerThread];
		for(int i = 0; i < boardsPerThread; ++i) {
			testSeeds[i]= new Seed(256*256);
		}
		int [] javaFitnesses = new int [boardsPerThread];
		int [][] testFitnesses = new int [2][boardsPerThread];
		System.out.println("Start");
		for(int j = 0; j < portNames.length; ++j) {
			System.out.println("new port");
			int jj = j;
			threads [j] = new Thread(() -> {
				for(int i = 0; i < boardsPerThread; ++i) {

					byte[] send = ConwayImplementation.toBytes(testSeeds[i].getSeed());
					//long Start = System.currentTimeMillis();
					int[] aa = new int [4];
					try {
						a[jj].writeBytes(send);
						aa = a[jj].readIntArray(4);
					} catch (SerialPortException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					int fitness = 0;
					fitness = fitness | aa[0];
					fitness = fitness | ((aa[1] << 8) | (fitness));
					fitness = fitness | ((aa[2] << 16) | (fitness));
					fitness = fitness | ((aa[3] << 24) | (fitness));

					testFitnesses[jj][i] = fitness;
					System.out.println(fitness);

				}
			});
			threads[j].start();
		}
		for(int i = 0; i < boardsPerThread; ++i) {
			Conway tester = new Conway(testSeeds[i], 256);
			tester.run(8000, 1, 0, 10);
			javaFitnesses[i] = tester.getViability();
		}
		
		for(int i = 0; i < portNames.length; ++i) {
			threads[i].join();
		}
		printLn("Total time " + (System.currentTimeMillis() - Starter));
		for(int i = 0; i < portNames.length; ++i) {
			a[i].closePort();
		}
		int errors = 0;
		System.out.println("Done");
		for(int i = 0; i < boardsPerThread; ++i) {
			int error = javaFitnesses[i] - testFitnesses[0][i];
			if(error != 0 && error != 9) {
				System.out.println("ERROR!! Iteration " + i);
				errors++;
				Conway BrokenSeed = new Conway(testSeeds[i], 256);
				BrokenSeed.csvOutput("Broken_Boards/brokenBoard" + i +".csv");
				printLn("Error " + i + ": FPGA says: " + testFitnesses[1][i] + " Java says: " + javaFitnesses[i]);
				printLn("Board saved as: brokenBoard" + i + ".csv");
			}
		}
		System.out.println("Total number of Errors " + errors + ".");
		PrintWriter pw = new PrintWriter(new File("Data_Files/LoopBack_Output/outputLoopBack1.txt"));
		pw.write(output.toString());
		pw.close();
	}

}
