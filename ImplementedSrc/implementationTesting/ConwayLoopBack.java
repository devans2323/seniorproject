package implementationTesting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class ConwayLoopBack {
	private Seed seed;
	private int boardSize;
	private int fitness;

	/*
	 * Constructor
	 * @param: seed is the Seed the game is built off of.
	 * @param: size is the number of rows and columns.
	 */
	public ConwayLoopBack(Seed seed, int size){
		this.seed = seed;
		this.boardSize = size;
		this.fitness = 0;
	}
	
	public void printBoard() {
		int k = 0; 
		for(int i = 0; i < boardSize; ++i) {
			for (int j = 0; j < boardSize; ++j) {
				if(seed.getSeed()[k]) {
					System.out.print("1 ");
				}else {
					System.out.print("0 ");
				}
				++k;
			}
			System.out.println();
		}
	}
	
	/*
	 * Method to print board to a csv output file.
	 */
	public void csvOutput() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("game.csv"));
		StringBuilder sb = new StringBuilder();
		int k = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if(seed.getSeed()[k]) {
					sb.append("1");
				}
				else {
					sb.append("0");
				}
				k++;
				if(j != boardSize - 1) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		pw.write(sb.toString());
		pw.close();
	}

	/*
	 * Method to get the initialization seed.
	 * return: Seed that the instance was built off of.
	 */
	public Seed getSeed() {
		return this.seed;
	}
	
	/*
	 * Method to get the number of rows and columns in the board.
	 * @return: number of rows and columns in the board.
	 */
	public int getSize() {
		return this.boardSize;
	}

	/*
	 * Method to return the viability of the last run.
	 * @return: returns the number of iterations until 
	 * 0 changes or steady state from the last time run was called.
	 */
	public int getFitness() {
		return this.fitness;
	}
	
	// https://stackoverflow.com/questions/26944282/java-boolean-to-byte-and-back
	public static byte[] toBytes(boolean[] input) {
		byte[] toReturn = new byte[input.length / 8];
		for (int entry = 0; entry < toReturn.length; entry++) {
			for (int bit = 0; bit < 8; bit++) {
				if (input[entry * 8 + bit]) {
					toReturn[entry] |= (128 >> bit);
				}
			}
		}

		return toReturn;
	}

	/*
	* The run method. This method runs the board through n iterations of Conway.
	* The loop breaks once the map stops changing, or reaches a constant steady
	* state.
	* @param: n is the number of iterations to run.
	*/
	@SuppressWarnings("static-access")
	public void run() throws SerialPortException {
		String[] portNames = SerialPortList.getPortNames();

		if (portNames.length == 0) {
			System.out.println("There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
			System.out.println("Press Enter to exit...");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		SerialPort a = new SerialPort(portNames[0]);
		a.openPort();
		a.setParams(9600, 8, 1, 0);
		byte [] send = toBytes(seed.getSeed());
		a.writeBytes(send);
		//LoopBack
		byte[] aa = a.readBytes(send.length);
		// Implementation
		// byte[] aa = a.readBytes(Integer.BYTES);
		int hold = 0;
		//Implementation
		//		for(int i = 0; i < Integer.SIZE; ++i) {
		//			hold += aa[i];
		//		}
		fitness = hold;
		// Testing code
		System.out.println("SEED");
		seed.printSeed();
		for(int i = 0; i < aa.length; ++i) {
			Integer print = new Integer(aa[i]);
			System.out.print(print.toBinaryString(print.intValue()) + " ");
		}
		a.closePort();
	}

}
