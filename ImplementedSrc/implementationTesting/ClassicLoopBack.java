package implementationTesting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.BitSet;

import conwaysGameOfLifeImplemented.ConwayImplementation;
import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;
import visualization.RunVis.ConwayVisualization;

public class ClassicLoopBack {

	private static StringBuilder output = new StringBuilder();


	public synchronized static void printLn(String a) {
		output.append(a + "\n");
	}

	/*
	 * Method to print to the console.
	 * @param: a is the string to be printed.
	 */
	public synchronized static void print(String a) {
		output.append(a);
	}

	//stackoverflow
	static boolean[] toBooleanArray(byte[] bytes) {
		BitSet bits = BitSet.valueOf(bytes);
		boolean[] bools = new boolean[bytes.length * 8];
		for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i+1)) {
			bools[i] = true;
		}
		return bools;
	}

	public static void main(String[] args) throws Exception  {
		String[] portNames = SerialPortList.getPortNames();

		if (portNames.length == 0) {
			printLn("There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
			printLn("Press Enter to exit...");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		SerialPort [] a = new SerialPort[portNames.length];
		for(int i = 0; i < portNames.length; ++i) {
			a[i] = new SerialPort(portNames[i]);
			a[i].openPort();
			a[i].setParams(1504000, 8, 1, 0);
		}


		//		int num = 32;
		//		byte [] send = new byte [num];//toBytes(seed.getSeed());
		//		String sending = "hgfedcbaWgfedcbahgfedcbahgfedcba";
		//		for(int i = 0; i < num; ++i) {
		//			send[i] = (byte)0;
		//		}

		boolean [] boolTest = new boolean [256*256];
		for(int i = 0; i < 5*256 + 40; ++i) {
			boolTest[i] = false;
		}
		boolTest[5*256+40] = true;
		for(int i = 5*256+41; i < 6*256 + 39; ++i) {
			boolTest[i] = false;
		}
		boolTest[6*256 + 39] = true;
		boolTest[6*256 + 40] = true;
		boolTest[6*256 + 41] = true;
		for(int i = 6*256 + 42; i < 12*256 + 39; ++i) {
			boolTest[i] = false;
		}
		boolTest[12*256 + 39] = true;
		boolTest[12*256 + 40] = true;
		boolTest[12*256 + 41] = true;
		for(int i = 12*256 + 42; i < 13*256 + 40; ++i) {
			boolTest[i] = false;
		}
		boolTest[13*256 + 40] = true;
		for(int i = 13*256+41; i < 256*256; ++i) {
			boolTest[i] = false;
		}
		boolTest[70*256] = true;
		boolTest[70*256+1] = true;
		boolTest[70*256+2] = true;

		//		for(int i = 0; i < 256; ++i) {
		//			for(int j = 0; j < 32; ++j) {
		//				System.out.print(boolTest[i*32 + j]);
		//			}
		//			System.out.println();
		//		}
		Thread [] threads = new Thread[portNames.length];
		long Starter = System.currentTimeMillis();
		int boardsPerThread = 1;
		Seed [] testSeeds = new Seed[boardsPerThread];
		for(int i = 0; i < boardsPerThread; ++i) {
			testSeeds[i]= new Seed(256*256);
		}
		int [] javaFitnesses = new int [boardsPerThread];
		int [][] testFitnesses = new int [2][boardsPerThread];
		System.out.println("Start");
		for(int j = 0; j < portNames.length; ++j) {
			System.out.println("new port");
			int jj = j;
			threads [j] = new Thread(() -> {
				for(int i = 0; i < boardsPerThread; ++i) {
//					Seed test;
//					final boolean deterministic = false;
//					if(!deterministic) {
//						test = new Seed(256*256);
//					}
//					else {
//						test = new Seed(boolTest);
//					}
					byte[] send = ConwayImplementation.toBytes(testSeeds[i].getSeed());
					//long Start = System.currentTimeMillis();
					int[] aa = new int [4];
					try {
						a[jj].writeBytes(send);
						aa = a[jj].readIntArray(4);
					} catch (SerialPortException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					

					//printLn("Iteration " + i + " FPGA Time: " + (System.currentTimeMillis() - Start));
					int fitness = 0;
					fitness = fitness | aa[0];
					fitness = fitness | ((aa[1] << 8) | (fitness));
					fitness = fitness | ((aa[2] << 16) | (fitness));
					fitness = fitness | ((aa[3] << 24) | (fitness));
					//printLn("Our official Fitness: " + tester.getViability());
					//printLn("Our " + jj + " boards says: " + fitness);
					//printLn("");
					testFitnesses[jj][i] = fitness;
					
//					if(fitness != tester.getViability() && fitness != tester.getViability() - 9) {
//						int error = fitness - tester.getViability();
//						System.out.println("Error of " + error);
//						System.out.println();
//
//					}
				}
			});
			threads[j].start();
		}
		for(int i = 0; i < boardsPerThread; ++i) {
			Conway tester = new Conway(testSeeds[i], 256);
			tester.run(8000, 1, 0, 10);
			javaFitnesses[i] = tester.getViability();
		}
		
		for(int i = 0; i < portNames.length; ++i) {
			threads[i].join();
		}
		printLn("Total time " + (System.currentTimeMillis() - Starter));
		//ConwayVisualization newVis = new ConwayVisualization();
		//newVis.runVisualization(256, 1000, 1000, 1000, 1, 2, 30000, 2000, test);
		//a.writeString(sending);
		//Thread.sleep(1000);
		//byte[] aa = a.readBytes(4);
		//for(int i = 0; i < aa.length; ++i) {
		//	System.out.println(aa[i]);
		//}
		//		for(int i = 0; i < 256; ++i) {
		//			for(int j = 0; j < 256; ++j) {
		//				if(boolTest[i*256 + j])
		//					System.out.print("1");
		//				else
		//					System.out.print("0");
		//			}
		//			System.out.println();
		//		}
		for(int i = 0; i < portNames.length; ++i) {
			a[i].closePort();
		}
		int errors = 0;
		System.out.println("Done");
		for(int i = 0; i < boardsPerThread; ++i) {
			int error = javaFitnesses[i] - testFitnesses[1][i];
			if(error != 0 && error != 9) {
				System.out.println("ERROR!! Iteration " + i);
				errors++;
				Conway BrokenSeed = new Conway(testSeeds[i], 256);
				BrokenSeed.csvOutput("Broken_Boards/brokenBoard" + i +".csv");
				printLn("Error " + i + ": FPGA says: " + testFitnesses[1][i] + " Java says: " + javaFitnesses[i]);
				printLn("Board saved as: brokenBoard" + i + ".csv");
			}
		}
		System.out.println("Total number of Errors " + errors + ".");
		PrintWriter pw = new PrintWriter(new File("Data_Files/LoopBack_Output/outputLoopBack1.txt"));
		pw.write(output.toString());
		pw.close();
	}

}
