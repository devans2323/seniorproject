package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

import conwaysGameOfLifeImplemented.ConwayRunImplementation;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import conwaysGameOfLifeSimulation.RunningConway.ConwaySimulation;
import jssc.SerialPortException;

public class ConwayMain {

	private static StringBuilder output = new StringBuilder();
	private static Random randNum = new Random();
	private static Random seededRandNum;
	private static int seed;

	/*
	 * Method to set seededRandNum
	 * @param: seed is  the value of the new seed
	 */
	public static void setSeededRandNum(int newSeed) {
		seed = newSeed;
		seededRandNum = new Random(seed);
	}
	/*
	 * Method to create a random boolean.
	 * @return: a random boolean.
	 */
	public static boolean randomBool() {
		int h = (int)(randNum.nextDouble()*2);
		if(h == 0) {
			return false;
		}
		else {
			return true;
		}
	}

	/*
	 * Method to create a seeded random boolean for repeatable results.
	 * @param i: a seed value for random.
	 * @return: a random boolean.
	 */
	public static boolean randomBool(int i) {
		if(i != seed || seededRandNum.equals(null)) {
			setSeededRandNum(i);
		}
		int h = (int)(seededRandNum.nextDouble()*2);
		if(h == 0) {
			return false;
		}
		else {
			return true;
		}
	}

	/*
	 * Method to create a random boolean for a specified chance of success.
	 * @param i: odds of true.
	 * @return: a random boolean.
	 */
	public static boolean randomBool(double i) {
		double h = randNum.nextDouble();
		if(h < i) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static int getRandNum(int populationSize) {
		return randNum.nextInt(populationSize);
	}

	/*
	 * Method to print to the console and create a new line.
	 * @param: a is the string to be printed.
	 */
	public synchronized static void printLn(String a) {
		output.append(a + "\n");
	}

	/*
	 * Method to print to the console.
	 * @param: a is the string to be printed.
	 */
	public synchronized static void print(String a) {
		output.append(a);
	}

	/*
	 * Method to print to a CSV file.
	 * @param: a is a 2 dimensional array holding the values to be written.
	 * @param: fileName is the desired filename.
	 */
	public static void printToCsv(int [][] a, String fileName) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Data_Files/" + fileName));
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				sb.append(a[i][j]);
				if(j != a[i].length - 1) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		pw.write(sb.toString());
		pw.close();
	}

	/*
	 * Method to print to a CSV file.
	 * @param: a is a 1 dimensional array holding the values to be written.
	 * @param: fileName is the desired filename.
	 */
	public static void printToCsv(double [] a, String fileName) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Data_Files/" +fileName));
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < a.length; i++) {
			sb.append(a[i]);
			if(i != a.length - 1) {
				sb.append(',');
			}
		}
		pw.write(sb.toString());
		pw.close();
	}

	public static void printToCsv(int [] a, String fileName) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Data_Files/" +fileName));
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < a.length; i++) {
			sb.append(a[i]);
			if(i != a.length - 1) {
				sb.append(',');
			}
		}
		pw.write(sb.toString());
		pw.close();
	}


	public static void printOutput() {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File("Data_Files/GAData/output.txt"));
			pw.write(output.toString());
			pw.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws FileNotFoundException, SerialPortException, InterruptedException {

		// Array with port names
		String [] ports = {"COM3", "COM6", "COM8" };
		// How often to print DataFiles
		int saveDataFile = 10;
		// Simulation or Implemenation
		boolean implementation = true;
		// Number of Games to be created.
		int populationSize = 1000;
		// number of rounds of breeding
		int generations = 160;
		// Size of squares of genes.
		int chunkSize = 16;
		//Odds that a cell is a alive in the initial board
		double oddsOfLife = .02;
		// Number of top boards passed to next generation.
		int survivors = (int) (0.01*populationSize);
		// Odds of mutation in the gamete
		double mutationRate = .001;

		// Number of rows and columns
		int boardSize = 256;
		// Number of runs per seed
		int runs = 8000;
		// Length of Seed
		int chromosomeLength = boardSize*boardSize;
		// int to tell how often to print board
		int printBoardEveryNGenerations = 4;
		// Boolean to print best board of each generation
		boolean printBoard = false;
		// Boolean to print the fitness of  each board in a generation
		boolean printFitness = false;
		// The maximum number of steps an oscillator can be and our algorithm still catches it.
		int oscillatorDepth = 10;
		// How far back we check steps. i.e. for a checked depth of 10, ten consecutive iterations
		// would need to have the same changes for a specific number of steps looking back.
		int checkedDepth = 20;
		// Sets the number of threads ideal to a particular computer.
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		// Boolean to print the statistics at the end of each generation.
		boolean printStats = true;
		// Number of breeds for each of the breeding members of our population.
		int breedsPerTopGun = 4;
		// Visualization variables
		// Horizontal pixels in screen, or desired window
		int screenSizeX = 1960;
		// Vertical pixels in screen or desired window
		int screenSizeY = 1080;
		// Sleep time to instantiate board
		int sleepTime = 1000;
		// Sleep time between iterations
		int waitTime = 20;
		// Number of bytes sent from FPGA
		int bytesToRead = 4;
		//Run visualization?
		boolean runVis = false;
		


		if(implementation) {
			ConwayRunImplementation.runImp(populationSize, boardSize, chromosomeLength, generations, chunkSize,
					runs, printBoardEveryNGenerations, printBoard, bytesToRead, survivors, breedsPerTopGun,
					screenSizeX, screenSizeY, sleepTime, waitTime, runVis, oddsOfLife, mutationRate, ports, saveDataFile );
		}else {
			ConwaySimulation.runSim(populationSize, boardSize, chromosomeLength, generations, chunkSize, runs,
					printBoardEveryNGenerations, printBoard, survivors, breedsPerTopGun, screenSizeX, screenSizeY,
					sleepTime, waitTime, numberOfThreads, oscillatorDepth, 		printFitness, checkedDepth, printStats, 
					oddsOfLife, mutationRate );
		}
		printOutput();	
	}

}
