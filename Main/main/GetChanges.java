package main;

import java.io.FileNotFoundException;

import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;

public class GetChanges {

	public static void main(String[] args) throws FileNotFoundException {
		Seed board = new Seed("Data_Files/GAData/InterestingBoards/GreatBoardFrom2%Run2.csv", 256);
		Conway a = new Conway (board, 256);
		a.run(16384, 1, 0, 10);
		ConwayMain.printToCsv(a.getChangeArray(), "GAData/InterestingBoards/bestBoardChanges.csv");
		System.out.println(a.getViability());
		
		

	}

}
