package main;

import java.io.FileNotFoundException;

import conwaysGameOfLifeImplemented.ConwayImplementation;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class ControlSeeds {
	

	public static void main(String[] args) throws SerialPortException, InterruptedException, FileNotFoundException {
		int numberOfSeeds = 10000;
		int boardSize = 256;
		DNA [] seeds = new DNA [numberOfSeeds];
		int [] fitnesses = new int[numberOfSeeds];
		for(int i = 0; i < numberOfSeeds; ++i) {
			seeds[i] = new DNA(boardSize*boardSize, .02);
		}
		String[] portNames = SerialPortList.getPortNames();
		int numberOfPorts = portNames.length;
		SerialPort [] ports = new SerialPort[numberOfPorts];
		for(int i = 0; i < numberOfPorts; ++i) {
			ports[i] = new SerialPort(portNames[i]);
			ports[i].openPort();
		}
		
		Thread [] threads = new Thread [numberOfPorts];
		int chunk = numberOfSeeds/numberOfPorts;
		int min = 0;
		int max = chunk;
		
		for(int i = 0; i < numberOfPorts; ++i) {
			final int ii = i;
			final int miny = min;
			final int maxy = max;
			threads[i] = new Thread(() -> {
				for(int j = miny; j < maxy; ++j) {
					ConwayImplementation runner = new ConwayImplementation (seeds[j].toSeed(), boardSize);
					try {
						runner.runWrite(ports[ii]);
						runner.runRead(ports[ii], 4);
					} catch (SerialPortException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					fitnesses[j] = runner.getFitness();
				}
			});
			min = max;
			if(i != numberOfPorts-2) {
				max = max + chunk;
			}else {
				max = numberOfSeeds;
			}
		}
		
		for(int i = 0; i < numberOfPorts; ++i) {
			threads[i].start();
		}
		for(int i = 0; i < numberOfPorts; ++i) {
			threads[i].join();
		}
		for(int i = 0; i < numberOfPorts; ++i) {
			ports[i].closePort();
		}
		ConwayMain.printToCsv(fitnesses, "GAData/FitnessScores/MichaelsData/RandomControlRun.csv");

	}

}
