package conwaysGameOfLifeSimulation.DNAAndSeedCreation;

import main.ConwayMain;

public class DNA {
	private int size;
	private boolean [] A;
	private boolean [] B;

	public DNA(int size, double oddsOfLife){
		this.size = size;
		A = new boolean [size];
		B = new boolean [size];
		createChromoA(oddsOfLife);
		createChromoB(oddsOfLife);
	}

	public DNA (boolean [] A, boolean [] B){
		this.size = A.length;
		this.A = A;
		this.B = B;
	}

	private void createChromoA(double oddsOfLife) {
		for(int i = 0; i < size; ++i) {
			A[i] = ConwayMain.randomBool(oddsOfLife);
		}
	}

	private void createChromoB(double oddsOfLife) {
		for(int i = 0; i < size; ++i) {
			B[i] = ConwayMain.randomBool(oddsOfLife);
		}
	}

	public Seed toSeed() {
		boolean [] seed = new boolean [size];
		for(int i = 0; i < size; ++i) {
			//seed[i] = A[i]||B[i];
			seed[i] = A[i]^B[i];
		}
		return new Seed(seed);
	}

	public void mutate(double mutationRate) {
		for(int i = 0; i < A.length; ++i) {
			if(ConwayMain.randomBool(mutationRate)) {
				A[i] = !A[i];
			}
			if(ConwayMain.randomBool(mutationRate)) {
				B[i] = !B[i];
			}
		}
	}
	public boolean[] getA() {
		return A;
	}

	public boolean[] getB() {
		return B;
	}
}
