package conwaysGameOfLifeSimulation.DNAAndSeedCreation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import main.ConwayMain;

public class Seed {

	// boolean array to hold the values of the board.
	private boolean [] seed;

	/*
	 * Constructor. Calls method to create the game seed.
	 * @param: size is the number of squares on board.
	*/ 
	public Seed(int size){
		this.seed = createSeed(size);
	}
	
	public Seed(int size, int SeedR){
		this.seed = createSeed(size, SeedR);
	}

	/*
	 * @Deprecated.
	 * Constructor. Instantiates seed array.
	 * @param: size is the number of squares on the board.
	 * @param: build is a boolean passed to declare intent to build
	 * the board as opposed to having the board generated.
	 */
	public Seed(int size, boolean build){
		this.seed = new boolean [size];
	}
	
	/*
	 * Contstructor. instantiated seed off of a passed in array.
	 * @param: boolean [] seed is the boolean [] representation of
	 * the seed.
	 */
	public Seed(boolean [] seed) {
		this.seed = seed;
	}
	/*
	 * Contstructor. instantiated seed off of a passed in array.
	 * TODO
	 */
	public Seed(String seedCSV, int boardSize) throws FileNotFoundException {
		File board = new File(seedCSV);
		Scanner boardIn = new Scanner(board);
		String line;
		seed = new boolean [boardSize*boardSize];
		int counter = 0;
		while(boardIn.hasNextLine()) {
			line = boardIn.nextLine();
			String [] noCommas = line.split(",");
			int [] values = new int [noCommas.length];
			for(int i = 0; i < noCommas.length; ++i) {
				 values[i] = Integer.parseInt(noCommas[i]);
				 if(values[i] == 0) {
					 seed[counter] = false;
				 }
				 else {
					 seed[counter] = true;
				 }
				 ++counter;
			}
		}
	}
	
	/*
	* Method to create the seed.
	* @param : takes in size. Size is the length of the seed.
	*/
	private boolean [] createSeed(int size){
		boolean [] seed = new boolean [size];
		for (int i = 0; i < size; i++) {
			seed[i] = ConwayMain.randomBool();
		}
		return seed;
	}
	
	private boolean [] createSeed(int size, int seedR){
		boolean [] seed = new boolean [size];
		for (int i = 0; i < size; i++) {
			seed[i] = ConwayMain.randomBool(i*seedR);
		}
		return seed;
	}

	/*
	* Method to get the seed array.
	* @return : a boolean array representing the board.
	*/ 
	public boolean [] getSeed() {
		return this.seed;
	}

	/*
	 * Method to change the Seed from the outside. Should not be used.
	 * @param : place is the index to change.
	 * @param : value is the new value of the index.
	 */
	public void changeSeed(int place, boolean value) {
		if(place < seed.length) {
			this.seed[place] = value;
		}
	}
	
	/*
	 *  Method to print seeds.
	 */
	public void printSeed() {
		for(int i = 0; i < seed.length; ++i) {
			if(seed[i]) {
				System.out.print("1 ");
			}else {
				System.out.print("0 ");
			}
		}
		System.out.println();
	}
}
