package conwaysGameOfLifeSimulation.Breeding;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import main.ConwayMain;

public class Gamete {
	
	private DNA dna;
	private boolean [] newChromo;
	private int boardSize;
	private int chunkSize;
	private double mutationRate;
	
	public Gamete(DNA dna, int chromoLength, int boardSize, int chunkSize, double mutationRate){
		this.dna = dna;
		this.newChromo = new boolean [chromoLength];
		this.boardSize = boardSize;
		this.chunkSize = chunkSize;
		this.mutationRate = mutationRate;
		createGamete();
		createGamete();
		
	}
	private int getIndex(int h, int i, int j, int k) {
		return h*boardSize*chunkSize + i*chunkSize + j*boardSize + k;
	}
	public void createGamete() {
		boolean [] A = dna.getA();
		boolean [] B = dna.getB();
		int boxesPerSide = boardSize / chunkSize;
		for(int h = 0; h < boxesPerSide; ++h) {
			for(int i = 0; i < boxesPerSide; ++i) {
				boolean AOrB = ConwayMain.randomBool();
				for (int j = 0; j < chunkSize; ++j) {
					for (int k = 0; k < chunkSize; ++k) {
						if(AOrB) {
							newChromo[getIndex(h,i,j,k)] = A[getIndex(h,i,j,k)];
						}
						else {
							newChromo[getIndex(h,i,j,k)] = B[getIndex(h,i,j,k)];
						}
					}
				}
			}
		}
		for(int i = 0; i < newChromo.length; ++i) {
			if(ConwayMain.randomBool(mutationRate)) {
				newChromo[i] = !newChromo[i];
			}
		}
	}

	public boolean [] getNewChromo() {
		return newChromo;
	}

}
