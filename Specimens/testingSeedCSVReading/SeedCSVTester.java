package testingSeedCSVReading;

import java.io.FileNotFoundException;

import conwaysGameOfLifeSimulation.ConwaysGameOfLife.Conway;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;
import visualization.RunVis.ConwayVisu;
import visualization.RunVis.ConwayVisualization;

public class SeedCSVTester {

	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		Seed brokenBoard = new Seed("Data_Files/Broken_Boards/brokenBoard19.csv", 256);
		ConwayForSeedDebuggin testSeed = new ConwayForSeedDebuggin(brokenBoard, 256);
		testSeed.run(8000, 1, 0, 10);
		int [] changes = testSeed.getChangeArray();
		int [] streaks = testSeed.getChangesRunStreaks();
		int[][] changeAndStreaks = new int[3][changes.length];
		for(int i = 0; i < changes.length; ++i) {
			changeAndStreaks[0][i] = i+1;
			changeAndStreaks[1][i] = changes[i];
			changeAndStreaks[2][i] = streaks[i];
		}
		ConwayMain.printToCsv(changeAndStreaks, "FPGADebugging/changeArray.csv");
		System.out.println(testSeed.getViability());
		
		ConwayVisualization a = new ConwayVisualization();
		a.runVisualization(256, 1000, 1000, 4000, 1, 10, 100, 30000, testSeed.getSeed());
	}

}
