package testingSeedCSVReading;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;

public class ConwayForSeedDebuggin {
	private Seed seed;
	private int boardSize;
	private boolean [][] currentBoard;
	private int viability;
	private int [][] countArray;
	private int [] change;
	private int lastChanges = 0;
	private int changes = 0;
	private boolean diedOfOscillation = false;
	private int [] changesRunStreaks;

	/*
	 * Constructor
	 * @param: seed is the Seed the game is built off of.
	 * @param: size is the number of rows and columns.
	 */
	public ConwayForSeedDebuggin(Seed seed, int size){
		this.seed = seed;
		this.boardSize = size;
		this.countArray = new int [size][size];
		this.currentBoard = new boolean [size][size];
		createBoard();
		this.viability = 0;
	}

	/*
	 * Method to print board.
	 */
	public void printBoard() {
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if(j != boardSize - 1) {
					if(currentBoard[i][j])
						ConwayMain.print("1,");
					else 
						ConwayMain.print("0,");

				}else {
					if(currentBoard[i][j])
						ConwayMain.printLn("1,");
					else 
						ConwayMain.printLn("0,");
				}
			}
		}
		ConwayMain.printLn(" ");
	}

	/*
	 * Method to print the count array.
	 */
	public void printCountArray() {
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if(j != boardSize - 1) {
					ConwayMain.print(countArray[i][j]  + " ");

				}else {
					ConwayMain.printLn(countArray[i][j] + "");
				}
			}
		}
		ConwayMain.printLn(" ");
	}

	/*
	 * Method to print board to a csv output file.
	 */
	public void csvOutput(String name) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Data_Files/" + name));
		StringBuilder sb = new StringBuilder();
		int k = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if(seed.getSeed()[k]) {
					sb.append("1");
				}
				else {
					sb.append("0");
				}
				k++;
				if(j != boardSize - 1) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		pw.write(sb.toString());
		pw.close();
	}

	/*
	 * Method to create the initial board from the seed value. Also
	 * initializes countArray.
	 */
	private void createBoard(){
		int k = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				countArray[i][j] = 0;
				currentBoard[i][j] = seed.getSeed()[k];
				k++;
			}
		}
		//printBoard();
	}

	/*
	 * Method to get the initialization seed.
	 * @return: Seed that the instance was built off of.
	 */
	public Seed getSeed() {
		return this.seed;
	}

	/*
	 * Method to get the number of rows and columns in the board.
	 * @return: number of rows and columns in the board.
	 */
	public int getSize() {
		return this.boardSize;
	}

	/*
	 * Method to return the viability of the last run.
	 * @return: returns the number of iterations until 
	 * 0 changes or steady state from the last time run was called.
	 */
	public int getViability() {
		return this.viability;
	}


	/*
	 * Method to get the change array for a current board.
	 * @return: an int [] holding the change data for each iteration.
	 */
	public int[] getChangeArray() {
		return this.change;
	}

	/*
	 * Method to get the changesRunStreaks variable
	 * @return: returns an int array with the number of times changes have remained constant
	 */
	public int[] getChangesRunStreaks() {
		return this.changesRunStreaks;
	}

	/*
	 * Method to get the number of changes in the last run.
	 * @return: Number of changes last time run was called.
	 */
	public int getLastChanges() {
		return this.lastChanges;
	}

	/*
	 * Method to determine whether a set of coordinates is safe
	 * to access our board with.
	 * @param: int j is the row
	 * @param: int k is the column
	 * @return: boolean true if the coordinates are safe.
	 */
	private boolean safe(int j, int k) {
		return (j != -1) && (k != -1) && (j != this.boardSize) && (k != this.boardSize);
	}

	/*
	 * Method to create the count board. The count board is 
	 * a representation of the actual board where each square
	 * holds the number of bordering living cells.
	 */
	private void counter() {
		for(int j = 0; j < boardSize; j++) {
			for(int k = 0; k < boardSize; k++) {
				int count = 0;
				if(safe(j-1, k-1)) {
					if(currentBoard[j-1][k-1]) {
						count++;
					}
				}
				if(safe(j-1, k)) {
					if(currentBoard[j-1][k]) {
						count++;
					}
				}
				if(safe(j-1, k+1)) {
					if(currentBoard[j-1][k+1]) {
						count++;
					}
				}
				if(safe(j, k-1)) {
					if(currentBoard[j][k-1]) {
						count++;
					}
				}
				if(safe(j, k+1)) {
					if(currentBoard[j][k+1]) {
						count++;
					}
				}
				if(safe(j+1, k-1)) {
					if(currentBoard[j+1][k-1]) {
						count++;
					}
				}
				if(safe(j+1, k)) {
					if(currentBoard[j+1][k]) {
						count++;
					}
				}
				if(safe(j+1, k+1)) {
					if(currentBoard[j+1][k+1]) {
						count++;
					}
				}
				countArray[j][k] = count;
			}
		}
	}

	/*
	 * This method checks a given square in the board to see if it is alive or
	 * dead in the next iteration. It also counts the changes of the current
	 * map.
	 * @param: j is the row of the square to be checked.
	 * @param: k is the column of the square to be checked.
	 */
	private void deadOrAlive(int j, int k) {
		if(currentBoard[j][k]) {
			if (countArray[j][k] != 2 && countArray[j][k] != 3) {
				currentBoard[j][k] = false;
				changes++;
			}
		}
		else {
			if (countArray[j][k] == 3) {
				currentBoard[j][k] = true;
				changes++;
			}
		}
	}

	/*
	 * Method that returns a boolean to represent the cause of death of a particular
	 * specimen during its run.
	 * return: boolean value of whether oscillation caused run to cut off.
	 */
	public boolean diedOfOscillation() {
		return diedOfOscillation;
	}

	/*
	 * The run method. This method runs the board through n iterations of Conway.
	 * The loop breaks once the map stops changing, or reaches a constant steady
	 * state.
	 * @param: n is the number of iterations to run.
	 * @param: oscillatorDepth is the maximum number of steps an oscillator can be and our algorithm still catches it.
	 * @param: id is the number of the board in the current generation.
	 * @param: checked Depth is the number of iterations that the board must oscillate for it to be declared dead.
	 */
	public void run(int runs, int oscillatorDepth, int id, int checkedDepth) {
		changesRunStreaks = new int [runs];
		int [] counter = new int [oscillatorDepth];
		this.change = new int [runs];
		boolean addChanges = true;
		boolean keepAdding = true;
		for(int i = 0; i < runs; i++) {
			changes = 0;
			counter();

			for (int j = 0; j < boardSize; j++) {
				for (int k = 0; k < boardSize; k++) {
					deadOrAlive(j,k);
				}
			}
			//Check for oscillators
			for(int j = 0; j < oscillatorDepth; ++j) {
				if(i > j) {
					if(changes == change[i - (j + 1)]) {
						//TODO counter values for each iteration
						++counter[j];
					}
					else {
						counter[j] = 0;
					}
				}
				else {
					break;
				}
			}
			changesRunStreaks[i] = counter[0];
			boolean breaker = false;
			for(int j = 0; j < oscillatorDepth; ++j) {
				if(counter[j] == checkedDepth) {
					addChanges = false;
					if(change[i - j - 1] != 0) {
						diedOfOscillation = true;
					}
					breaker = true;
				}
			}
			if(breaker) {
				keepAdding = false;
			}
			change[i] = changes;
			if(keepAdding) {
				viability++;
			}
		}
		if(addChanges) {
			viability += changes;
		}
	}


	/* 
	 * Not up to date.
	 * Method to print each iteration.
	 * @deprecated: once proven this method became unnecessary.
	 * @param: n the number of runs.
	 */
	public void printRun(int n) throws FileNotFoundException {
		ConwayMain.printLn("New Print Run");
		printBoard();
		int counter = 0;
		boolean addChanges = true;
		for(int i = 0; i < n; i++) {
			changes = 0;
			counter();
			for (int j = 0; j < boardSize; j++) {
				for (int k = 0; k < boardSize; k++) {
					deadOrAlive(j,k);
				}
			}
			if (changes == lastChanges) {
				++counter;
			}
			else {
				counter = 0;
			}
			if(counter == 10) {
				addChanges = false;
				break;
			}
			printBoard();
			lastChanges = changes;
			viability++;
		}
		if(addChanges) {
			viability += changes;
		}
	}
}
