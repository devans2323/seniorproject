package visualization.TestVis;

import java.awt.Color;
import java.awt.EventQueue;
import java.io.FileNotFoundException;

import javax.swing.JPanel;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.DNA;
import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import main.ConwayMain;
import visualization.Conway.ConwayRunVis;
import visualization.RunVis.ConwayVisu;
import visualization.RunVis.ConwayVisualization;

public class testVis {
	private static ConwayVisu frame;

	public static void updateFromAfar(int size, boolean [][] a) {
		update(a, size, frame.getButtons());
	}

	public static void update(boolean a[][], int size, JPanel [][] buttons) {
		for(int i = 0; i < size; ++i) {
			for(int j = 0; j < size; ++j) {
				if(a[i][j]) {
					if(!buttons[i][j].getBackground().equals(Color.GREEN)) {
						buttons[i][j].setBackground(Color.GREEN);
					}
				}else {
					if(!buttons[i][j].getBackground().equals(Color.GRAY)) {
						buttons[i][j].setBackground(Color.GRAY);
					}
				}
			}
		}
	}

	public static void main(String[] args) throws InterruptedException, FileNotFoundException {
		ConwayVisualization test = new ConwayVisualization();
		int size = 256;
		int screenSizeX = 1960;
		int screenSizeY = 1080;
		int runs = 50000;
		int oscillatorDepth = 1;
		int checkedDepth = 10;
		long sleepTime = 700;
		long waitTime = 30000;
		//Seed newConway = new Seed("Data_Files/GAData/bestBoard.csv", 256);
		//test.runVisualization(size, screenSizeX, screenSizeY, runs, oscillatorDepth, checkedDepth, sleepTime, waitTime, newConway);
		DNA newConway = new DNA(size*size, .5);
		test.runVisualization(size, screenSizeX, screenSizeY, runs, oscillatorDepth, checkedDepth, sleepTime, waitTime, newConway.toSeed());
	}
}
