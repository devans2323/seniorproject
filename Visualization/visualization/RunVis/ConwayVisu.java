package visualization.RunVis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ConwayVisu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	private JPanel [][] buttons;
	public JPanel [][] getButtons(){
		return buttons;
	}
	public void setButtons(JPanel[][] buttons) {
		this.buttons = buttons;
	}

	/**
	 * Create the frame.
	 */
	public ConwayVisu(boolean [][] a, int size, int width, int height, int frameSizeX, int frameSizeY) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, frameSizeX, frameSizeY);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setBackground(Color.WHITE);
		setLayout(null);
		buttons = new JPanel [size][size];
		for(int i = 0; i < size; ++i) {
			for(int j = 0; j < size; ++j) {
				setSize(1024, 1024);
				buttons[i][j] = new JPanel();
				buttons[i][j].setBounds(i*width, j*height, width, height);
				if(a[i][j]) {
					buttons[i][j].setBackground(Color.GREEN);
				}else {
					buttons[i][j].setBackground(Color.GRAY);
				}
				add(buttons[i][j]);
			}
		}
	}

}
