package visualization.RunVis;

import java.awt.Color;
import java.awt.Frame;

import javax.swing.JPanel;

import conwaysGameOfLifeSimulation.DNAAndSeedCreation.Seed;
import visualization.Conway.ConwayRunVis;

public class ConwayVisualization {
	
	private static ConwayVisu frame;
	
	public ConwayVisualization() {
	}

	public static void updateFromAfar(int size, boolean [][] a) {
		update(a, size, frame.getButtons());
	}

	public static void update(boolean a[][], int size, JPanel [][] buttons) {
		for(int i = 0; i < size; ++i) {
			for(int j = 0; j < size; ++j) {
				if(a[i][j]) {
					if(!buttons[i][j].getBackground().equals(Color.GREEN)) {
						buttons[i][j].setBackground(Color.GREEN);
					}
				}else {
					if(!buttons[i][j].getBackground().equals(Color.GRAY)) {
						buttons[i][j].setBackground(Color.GRAY);
					}
				}
			}
		}
	}

	public void runVisualization(int size, int screenSizeX, int screenSizeY, int runs, int oscillatorDepth,
			int checkedDepth, long sleepTime, long waitTime, Seed newConway) throws InterruptedException{
		int width = screenSizeX/size;
		int height = screenSizeY/size;
		ConwayRunVis board = new ConwayRunVis(newConway, size);
		boolean [][] a = board.getBoard();
		Thread tr1 = new Thread (new Runnable() {
			public void run() {
				try {
					frame = new ConwayVisu(a, size, width, height, screenSizeX, screenSizeY);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tr1.start();
		tr1.join();
		Thread.sleep(waitTime);
		board.run(runs, oscillatorDepth, checkedDepth, sleepTime);
		System.out.println(board.getViability());
	}
}
